--
-- Struktura tabeli dla tabeli `gps`
--

CREATE TABLE IF NOT EXISTS `gps` (
  `id` varchar(32) COLLATE utf8_bin NOT NULL,
  `latitude` float(12,9) NOT NULL,
  `longtitude` float(12,9) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Struktura tabeli dla tabeli `files`
--

CREATE TABLE IF NOT EXISTS `files` (
  `name` varchar(64) COLLATE utf8_bin NOT NULL,
  `file` mediumblob NOT NULL,
  `gpsID` varchar(32) COLLATE utf8_bin NOT NULL,
  `cityID` varchar(32) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Struktura tabeli dla tabeli `city`
--

CREATE TABLE IF NOT EXISTS `city` (
  `id` varchar(32) COLLATE utf8_bin NOT NULL,
  `name` text COLLATE utf8_bin NOT NULL,
  `population` int(11) NOT NULL,
  `country` varchar(32) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;