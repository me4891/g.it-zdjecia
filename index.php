<meta charset="UTF-8"/>
<form action="<?php echo $_SERVER['SCRIPT_NAME'] ?>" method="post" enctype="multipart/form-data">
	<input type="file" name="upload">
	<input type="submit">
</form>
	
<?php
$config['database']['dbhost']='localhost';
$config['database']['dbuser']='<<DB_USER>>';
$config['database']['dbpass']='<<DB_PASSWORD>>';
$config['database']['dbtable']='<<DB_TABLE>>';
$db=new mysqli($config['database']['dbhost'], $config['database']['dbuser'], $config['database']['dbpass'], $config['database']['dbtable']);

class Picture {
	var $GoogleApiKey='<<API_KEY>>';
	
	protected $GpsLatitude='';
	protected $GpsLongtitude='';
	protected $CityName='';
	protected $CityPopulation='';
	protected $CityCountry='';
	
	public function getCurl($url) {
		$ch=curl_init();
		$data=array();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_HEADER, true);
		curl_setopt($ch, CURLOPT_NOBODY, true);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($ch, CURLOPT_MAXREDIRS, 15);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 60);
		curl_setopt($ch, CURLOPT_FAILONERROR, true);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		$res=curl_exec($ch);
		if(!$res) { echo "<hr>".curl_error($ch).'<hr>'; }
		curl_close($ch);
		return $res;
	}
	
	function Save() {
		global $db;
		if(($this->type!='image/jpeg') AND ($this->type!='image/png')) {
			echo 'Zdjęcie musi być w formacie jpg lub png<br/>';
			break;
		}
		else {
			$GpsID=md5($this->GpsLatitude.$this->GpsLongtitude);
			$sqli=$db->query("REPLACE INTO gps(id, latitude, longtitude) VALUES ('".$GpsID."', ".$this->GpsLatitude.", ".$this->GpsLongtitude.");");
		
			$CityID=md5($this->CityName.$this->CityCountry.$this->CityPopulation);
			$db->query("REPLACE INTO city(id, name, population, country) VALUES ('".$CityID."', '".$this->CityName."', '".$this->CityPopulation."', '".$this->CityCountry."');");
		
			$db->query("REPLACE INTO files (name, file, gpsID, cityID) VALUES ('".$this->filename."','".$this->tmp_file."','".$GpsID."','".$CityID."');");
			echo 'Plik zapisany';
		}
	}

	function getPictures() {
		global $db;
		$query="SELECT f.name, f.file, c.name as cityname, c.population, c.country, g.latitude, g.longtitude FROM files f LEFT JOIN city c ON f.cityID=c.id LEFT JOIN gps g ON f.gpsID=g.id";
		$sql=$db->query($query);
		$this->numrows=$sql->num_rows;
		$this->sql=$sql;
	}


	function showPicture() {
		if(!$this->numrows) return 0;
		if($row=mysqli_fetch_array($this->sql)) {
			$r['name']=$row['name'];
			$r['cityname']=$row['cityname'];
			$r['country']=$row['country'];
			$r['population']=$row['population'];
			$r['file']=$row['file'];
			$r['latitude']=$row['latitude'];
			$r['longtitude']=$row['longtitude'];
			$r['miejsce']=$row['latitude'].', '.$row['longtitude'];
			
			return $r;
		} 
		else return 0;
	}
	
}

class getInfo extends Picture {
	public function getInfo($file) {
		if($file) {
			$this->filename=$file['name'];
			$this->type=$file['type'];
			$this->tmp_name=$file['tmp_name'];
			$this->tmp_file=addslashes(file_get_contents($file['tmp_name']));
			
			$this->getLatLonFromExif();
			$this->getReverseGeododingInfo();
			$this->getPopulation($this->CityName);
			
			$this->save();
		}
	}
	public function getExif() {
		$exif=exif_read_data($this->tmp_name, 0, true);
		if($exif===false) {
			echo 'Brak Exifa<br/>';
			break;
		}
		else {
			return $exif;
		}
	}
	function getLatLonFromExif() {
		$exif=$this->getExif();
		if(!isset($exif['GPS'])) {
			echo 'Brak danych Geolokalizacyjnych w Exif<br/>';
			break;
		}
		else {
			$this->GpsLongtitude=$this->getGps($exif['GPS']["GPSLongitude"], $exif['GPS']['GPSLongitudeRef']);
			$this->GpsLatitude=$this->getGps($exif['GPS']["GPSLatitude"], $exif['GPS']['GPSLatitudeRef']);
		}
	}
	private function getGps($exifCoord, $hemi) {
		$degrees=count($exifCoord) > 0 ? $this->gps2Num($exifCoord[0]) : 0;
		$minutes=count($exifCoord) > 1 ? $this->gps2Num($exifCoord[1]) : 0;
		$seconds=count($exifCoord) > 2 ? $this->gps2Num($exifCoord[2]) : 0;
		$flip=($hemi=='W' OR $hemi=='S') ? -1 : 1;
		return $flip * ($degrees + $minutes / 60 + $seconds / 3600);
	}
	private function gps2Num($coordPart) {
		$parts=explode('/', $coordPart);
		if (count($parts)<=0)
			return 0;
		if (count($parts)==1)
			return $parts[0];
		return floatval($parts[0]) / floatval($parts[1]);
	}
	private function getReverseGeododingInfo() {
		$get_API='https://maps.googleapis.com/maps/api/geocode/json?latlng=';
		$get_API.=round($this->GpsLatitude,4).",";
		$get_API.=round($this->GpsLongtitude,4);
		$jsonfile=file_get_contents($get_API.'&result_type=country|locality&key='.$this->GoogleApiKey.'&sensor=false');
		$jsonarray=json_decode($jsonfile,true);
		
		if(isset($jsonarray['results'][0]['address_components'][0]['long_name'])) {
			$this->CityName=$jsonarray['results'][0]['address_components'][0]['long_name'];
			$this->CityCountry=$jsonarray['results'][1]['address_components'][0]['long_name'];
			$this->CityGlobalCode=$jsonarray['plus_code']['global_code'];
		}
		else {
			return('Brak danych reverse geolocation');
			break;
		}
	}
	function getPopulation($city) {
		$city=str_replace(' ','_',$city);
		$txt=$this->getCurl('https://pl.wikipedia.org/wiki/'.$city);
		
		$temp=explode('Populacja',$txt);
		$txt=$temp[1];
		$temp=explode('Strefa numeracyjna',$txt);
		$txt=$temp[0];
		$txt=preg_replace('/\s+/', '', $txt);
		$temp=explode('<td><br/>',$txt);
		$txt=$temp[1];
		preg_match('/([0-9.]+)([^0-9]+)/',$txt,$matches);
		$txt=str_replace('.','',$matches[1]);
		if(is_numeric($txt)) { 
			//echo $city.': '.$txt.'<br/>';
			$this->CityPopulation=$txt;
		}
		else { 
			echo "Brak danych o popoulacji";
			$this->CityPopulation=0;
		}
	}
}


if($_FILES['upload']) {
	$info=new getInfo($_FILES['upload']);
}


$pic=new Picture();
$pic->getPictures();
echo '<table>';
while($row=$pic->showPicture()) {
	//echo '<pre>'; print_r($row); echo '</pre>';
	echo '<tr>';
	echo ' <td><img src="data:image/jpeg;base64,'.base64_encode($row['file']).'" width="200"></td>';
	echo ' <td>nazwa pliku: '.$row['name'].'<br/>współrzędne zrobienia zdjęcia: '.$row['miejsce'].'<br/>miasto: '.$row['cityname'].', '.$row['country'].'<br/>populacja: '.$row['population'].'</td>';
	echo '</tr>';
}
echo '</table>';

?>